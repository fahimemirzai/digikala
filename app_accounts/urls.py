from django.urls import path, include

from . import views
from rest_framework.authtoken import views as rest_views


urlpatterns = [

    path('registration', views.register_view),
    path('ProfileView', views.ProfileView),
    path('EditProfileView', views.EditProfileView),

    path('api-token-auth', rest_views.obtain_auth_token),
    path('api-auth', include('rest_framework.urls')),

    path('show-basket', views.show_basket_item_view),
    path('add-basket', views.add_basket_view),

]
