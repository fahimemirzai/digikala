# Generated by Django 2.2.12 on 2020-04-27 07:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_accounts', '0004_basketitem_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='basketitem',
            name='price',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='basketitem',
            name='count',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
